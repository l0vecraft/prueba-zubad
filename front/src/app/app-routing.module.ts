import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateComponent } from './create/create.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { SearchComponent } from './search/search.component';
import { CategoryComponent } from './category/category.component';
import { EditProductComponent } from './edit-product/edit-product.component';


const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'create', component:CreateComponent},
  {path:'search/:search', component:SearchComponent},
  {path:'category/:category', component:CategoryComponent},
  {path:'product/:id', component:ProductDetailComponent},
  {path:'edit/:id', component:EditProductComponent},
  {path:'**', component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
