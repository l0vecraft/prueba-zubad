import { Component, OnInit } from '@angular/core';
import { Helpers } from '../services/helpers.services';
import { Product } from '../models/product';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[Helpers]
})
export class HomeComponent implements OnInit {
  public product:Product[];
  public url:string;
  constructor(
    private _helpers:Helpers
  ) { 
    this.url = this._helpers.urlBase;
    this.product = new Array<Product>();
  }

  ngOnInit() {
    this.getUsers();
  }
  getUsers(){
    this._helpers.getProducts().subscribe(
      result=>{
        if(result.products!==[''] || result.products!==undefined){
          this.product = result.products;
          console.log(this.product);
        }
      },
      error=>{
        console.log(<any>error);
      }
    );
  }
}
