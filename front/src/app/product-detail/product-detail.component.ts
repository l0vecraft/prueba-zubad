import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from "@angular/router";
import { Helpers } from '../services/helpers.services';
import { Product } from '../models/product';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
  providers:[Helpers]
})
export class ProductDetailComponent implements OnInit {
  public id: Number;//hay que cambiarlo para mi api
  public product:Product;
  public cantidadComprar: number;
  public url: String;

  constructor(
    private _activateRoute: ActivatedRoute,
    private _router: Router,
    private _helper: Helpers
  ) { 
    this.url = this._helper.urlBase;
    this.id = this._activateRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.getProduct();
  }

    getProduct(){
      this._helper.getProduct(this.id).subscribe(
        result=>{
          this.product = result.product;
          console.log(this.product);
        },
        error=>{
          console.log(error);
        }
      )
    }
  
    deleteProduct(id){
      this._helper.deleteProduct(id).subscribe(
        res=>{
          if(res){
            this._router.navigate(['/']);
          }
        },
        error=>{
          console.log(error);
        }
      );
    }
}
