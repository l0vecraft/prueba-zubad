import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public search:String;
  public formGroup: FormGroup;
  constructor(
    private _formBuilder:FormBuilder
  ){
    this.search='';
  }

  public ngOnInit(){
    this.searchForm();
  }
  searchForm(){
    this.formGroup = this._formBuilder.group({
      search:[this.search, Validators.pattern('[a-zA-Z0-9]')]
    })
  }
  searchProduct(){
    this.search= this.formGroup.value.search;
    console.log(this.search);
    window.location.href='/search/'+this.search;
  }
}
