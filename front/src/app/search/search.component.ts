import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";
import { Product } from '../models/product';
import { Helpers } from '../services/helpers.services';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers:[Helpers]
})
export class SearchComponent implements OnInit {
  public searchItem:String;
  public products: Product[];
  public categories: String[];
  public url:String;

  constructor(
    private _activeRoute: ActivatedRoute,
    private _helpers: Helpers
  ) {
    this.searchItem = this._activeRoute.snapshot.params.search;
    this.categories = new Array<String>();
    this.products = new Array<Product>();
    this.url = this._helpers.urlBase;
   }

  ngOnInit() {
   this.getProducts();
   this.getCategories();
  }
  getProducts(){
    this._helpers.getProductsSearch(this.searchItem).subscribe(
      result=>{
        if(result){
          this.products = result.data;
        }
      },
      error=>{
        console.log(error);
      }
    );
  }
  getCategories(){
    this._helpers.getCategories().subscribe(
      result=>{
        if(result){
          this.categories = result.products;
        }
      },
      error=>{
        console.log(error);
      }
    );
  }
}
