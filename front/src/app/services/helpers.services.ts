import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { stringify } from 'querystring';

@Injectable()
export class Helpers{
    public urlBase = 'http://localhost:3809/api'
    // public url:string;
    constructor(
        public _http: HttpClient
    ){
    }

    addProduct(product:Product){
        let data = JSON.stringify(product);
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.post(this.urlBase+'/addProduct',data,{headers:headers});
    }
    getCategories():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.get(this.urlBase+'/categories',{headers:headers});
    }
    getProducts():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.get(this.urlBase+'/products',{headers:headers});
    }
    getProduct(id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.get(this.urlBase+'/product/'+id, {headers:headers});
    }
    getProductCategory(category:String):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.get(this.urlBase+'/products/'+category,{headers:headers});
    }
    getProductsSearch(product:String):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.get(this.urlBase+'/search/'+product,{headers:headers});
    }

    deleteProduct(id):Observable<any>{
        let headers = new HttpHeaders().set('Contet-Type','application/json');
        headers.append('Access-Control-Allow-Origin','*');
        return this._http.post(this.urlBase+'/product/'+id,{headers:headers});
    }
    updateProduct(product):Observable<any>{
        let params = JSON.stringify(product);
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.put(this.urlBase+'/product/'+product._id,params,{headers:headers});
    }
}

