import { Injectable } from "@angular/core";
import { Helpers } from './helpers.services';


@Injectable()
export class UploadService{
    public url:String
    constructor(
        private _helper:Helpers
    ){
        this.url=  this._helper.urlBase;
    }

    //* esta sera una funcion para que se cargue la imagen del producto, justo cuando se crea
    makeFileRequest(url,params,files,name){
        return new Promise((resolve,reject)=>{
            let formData:any = new FormData();
            var xhr = new XMLHttpRequest();

            for (let i = 0; i < files.length; i++) {
                formData.append(name,files[i],files[i].name);
            }
            xhr.onreadystatechange =function(){
                if(xhr.readyState==4){
                    if(xhr.status==200){
                        resolve(JSON.parse(xhr.response));
                    }else{
                        reject(xhr.response);
                    }
                }
            }
            xhr.open('POST',url,true);
            xhr.send(formData);
        });
    }
}