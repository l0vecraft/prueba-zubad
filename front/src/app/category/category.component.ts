import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Helpers } from '../services/helpers.services';
import { Product } from '../models/product';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers:[Helpers]
})
export class CategoryComponent implements OnInit {
  public products: Product[];
  public categories: String[];
  public url:String;
  public category: String;

  constructor(
    private _helpers:Helpers,
    private _activeRoute: ActivatedRoute
  ) {
    this.url = this._helpers.urlBase;
    this.category = this._activeRoute.snapshot.params.category;
    this.products = Array<Product>();
  }

  ngOnInit() {
    this.getCategories();
    this.getProducts();
    console.log(this.category);
  }

  getCategory(c){
    this.category = c;
    window.location.href='/category/'+this.category;
    this.getProducts();
  }
  getCategories(){
    this._helpers.getCategories().subscribe(
      result=>{
        if(result){
          this.categories = result.products;
        }
      },
      error=>{
        console.log(error);
      }
    );
  }
  getProducts(){
    this._helpers.getProductCategory(this.category).subscribe(
      res=>{
        if(res){
          this.products = res.products;
          console.log(this.products);
        }
      },
      error=>{
        console.log(error);
      }
    );
  }
}
