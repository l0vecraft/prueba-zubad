import { Component, OnInit } from '@angular/core';
import { Helpers } from '../services/helpers.services';
import { UploadService } from '../services/upload.service';
import { Product } from '../models/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [Helpers,UploadService]
})
export class CreateComponent implements OnInit {
  public title:string;
  public product:Product;
  public formGroup: FormGroup;
  public fileToUpload: Array<File>;

  constructor(
    private _productService:Helpers,
    private _formBuilder: FormBuilder,
    private _uploadService: UploadService
  ) { 
    this.title='Crear producto';
    this.product= new Product([''],'','','','','',0,0,'');
  }

  ngOnInit() {
    this.registerProduct();
  }

  fileChangeEvent(fileEvent:any){
    this.fileToUpload = <Array<File>>fileEvent.target.files;
    console.log(fileEvent);
  }

  registerProduct(){
    this.formGroup = this._formBuilder.group({
      nombre: ['',Validators.required],
      descript: ['', Validators.required],
      category:['', Validators.required],
      precio: ['', Validators.required,],
      cantidad: ['',Validators.required],
      smallImage: ['']
    });
  }
  addProduct(){
    this._productService.addProduct(this.product).subscribe(
      res=>{
        if(res){
          //subir imagen
          this._uploadService.makeFileRequest(this._productService.urlBase+'/uploadImage/'+res['product']._id,[],this.fileToUpload,'image')
          .then((resul:any)=>{
            console.log(resul);
          });
          
          window.location.href='/';
        }
      },
      error=>{
        console.log(error);
      }
    );
  }
}
