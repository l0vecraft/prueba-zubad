export class Product {
    constructor(
        public images:[String],
        public _id: String,
        public nombre:String,
        public descript: String,
        public smallDescript: String,
        public category:String,
        public precio:Number,
        public cantidad:Number,
        public smallImage: String
    ) {
    }
}