import { Component, OnInit } from '@angular/core';
import { Helpers } from '../services/helpers.services';
import { ActivatedRoute } from "@angular/router";
import { UploadService } from '../services/upload.service';
import { Product } from '../models/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css'],
  providers: [Helpers, UploadService]
})
export class EditProductComponent implements OnInit {
  public product: Product;
  public id: String;
  public formGroup: FormGroup;
  public fileToUpload: Array<File>;

  constructor(
    private _productService: Helpers,
    private _formBuilder: FormBuilder,
    private _uploadService: UploadService,
    private _activateRoute: ActivatedRoute
  ) {
    this.id = this._activateRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.getProduct();
    this.registerProduct();
  }

  getProduct() {
    this._productService.getProduct(this.id).subscribe(
      result => {
        this.product = result.product;
        console.log(this.product);
      },
      error => {
        console.log(error);
      }
    )
  }

  registerProduct() {
    this.formGroup = this._formBuilder.group({
      nombre: ['', Validators.required],
      descript: ['', Validators.required],
      category: ['', Validators.required],
      precio: ['', Validators.required,],
      cantidad: ['', Validators.required],
      smallImage: ['']
    });
  }
  updateProduct(){
    this._productService.updateProduct(this.product).subscribe(
      result=>{
        window.location.href='/';
      },
      error=>{
        console.log(error);
      }
    );
  }
  fileChangeEvent(fileEvent:any){
    this.fileToUpload = <Array<File>>fileEvent.target.files;
    console.log(fileEvent);
  }
}
