# pruebaZabud
Este repositorio fue creado para cargar la prueba técnica impuesta por Zuba.
La prueba consistió en hacer un **CRUD** sencillo:
1. Lista de productos
2. Buscar productos
3. Detalle del producto
4. Agregar productos
6. Eliminar productos
7. Mostrar los productos por categoría
8. Editar productos

## Tecnologías utilizadas
El desarrollo de esta aplicación se realizó en el sistema operativo ubuntu 18.04 (x64), también utilizado:
* **CLI angular**: 8.2.2
* **NodeJs**: 10.16.2 (npm: '6.11.1')
* **mongodb**: versión de db v4.2.0
* **BootStrap**: 4.3.0

# Interfaz
En la parte frontend, se utilizó la última versión de angular, hasta ahora es 8, las bibliotecas que se utilizaron fueron:
* **NgModule**: para manipulación de datos bidireccional
* **HttpClient**: para realizar solicitudes a nuestro backend, también se utilizó HttpHeaders, para la configuración correcta de los encabezados
* **ReactiveFormsModule, FormsModule**: para manipulación de datos y validación de formularios

para ejecutar la parte frontend correctamente, **Clone** el repositorio en cualquier carpeta o directorio que desee, 
luego abra un terminal dentro de esa carpeta y ejecute el comando ***npm install*** 
esto instalará las dependencias que están dentro del archivo de carpeta **package.json**.

## Rutas 
En el archivo app-routing.module.ts, se encuentran las siguientes rutas 
* ' ': Inicio 
* 'create': crear un nuevo producto 
* 'search/<nombreProduct>':, buscar un producto por su nombre 
* 'category/:<categoría>', buscar productos por categoría 
*' edit/:id ', actualizar un producto 
*' xx ', una ruta para manejar errores, sería 404, por defecto redirige a inicio

# Backend 
la parte del backend se realizó con NodeJS, utilizando las siguientes bibliotecas para facilitar el desarrollo:
* **express**: para el desarrollo de la API Rest 
* **body-parser**: para convertir solicitudes a objetos json 
* **connect-multiparty**: para facilitar la carga de archivos al servidor 
* **mongoose**: para facilitar la conexión y manipulación de los datos de la base
de datos mongodb 

**Clone** el repositorio en cualquier carpeta o directorio que desee, 
luego abra un terminal dentro de esa carpeta y ejecute el comando ***npm install*** 
esto instalará las dependencias que están dentro del archivo de carpeta **package.json**.

## Controllers
there are 2 controllers inside this folder.
* **category**: quién tiene los métodos para guardar todas las categorías en el producto DB.
* **product** : quién tiene todos los métodos, para agregar, editar, eliminar, listar, detalles y buscar un producto.

## Models
tienen la estructura respectiva que deben llevar la categoría y los objetos del producto. 
Hay 2 modelos: 
* **Category**: tiene un título y una identificación 
* **Product**: tiene un nombre, descripción, pequeña Descripción (implementación futura), imágenes (implementación futura), smallImage, price, count, category 

## Uploads
Este es el lugar donde irán todas las imágenes cargadas de los productos
