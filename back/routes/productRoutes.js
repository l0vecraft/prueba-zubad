'use strict'

let express = require('express');
let productController = require('../controllers/product');

let router = express.Router();//quien ayudara a generar las rutas
let multipart = require('connect-multiparty');//libreria para subir archivos
let multipartMiddle = multipart({uploadDir:'./uploads'})//configurando middleware para las imagenes que se suban

//home
router.get('/home',productController.home);
//agregar producto
router.post('/addProduct', productController.add);
//subir una imagen
router.post('/uploadImage/:id',multipartMiddle,productController.uploadImage);
// router.post('/uploadImages/:id',multipartMiddle,productController.uploadImages);
//listar todos los productos
router.get('/products', productController.listProduct);
//buscar un producto
router.get('/product/:id?',productController.getProduct);
//actualizar un producto
router.put('/product/:id?', productController.updateProduct);
//eliminar producto
router.post('/product/:id', productController.removeProduct);
//buscar por categoria
router.get('/products/:category', productController.findByCategory);
//subir imagen
router.get('/image/:image',productController.getImage);
//buscar por nombre
router.get('/search/:product?', productController.search);
//listar categorias
router.get('/categories', productController.getCategories);
//api.post('/test', productController.test);
module.exports= router

//params obligatorio= :
//params opcional = ?