'use strict'

let express = require('express');
let cartController = require('../controllers/cart');

let router = express.Router();

router.post('/addProduct/:id/:cantidad',cartController.addProduct);
router.post('/deleteProduct/:id/:cantidad',cartController.deleteProduct);

module.exports = router;