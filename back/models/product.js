'use strict'

let mongoose = require('mongoose');
//definiendo el esquema del modelo
let Schema = mongoose.Schema;

let productSchema = Schema({
    nombre:String,
    descript: String,
    smallDescript: String,
    precio:Number,
    cantidad:Number,
    smallImage:String,
    category:String,
    images:[String]
});
//este indice es para poder realizar busquedas de texto semi parcial
// productSchema.index({nombre:'text', category:'text'});
//exportamos el modulo
module.exports = mongoose.model('Product',productSchema);