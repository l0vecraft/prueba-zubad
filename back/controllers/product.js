'use strict'

let productModel = require('../models/product');
let CategoryModel= require('../models/category');
let fs = require('fs');
let path = require('path');

var productController = {
    home: (req,res)=>{
        return res.status(200).send({
            message:'home'
        });
    },
    add:(req,res)=>{
        let params = req.body;
        let category = new CategoryModel();
        let newProduct = new productModel();

        newProduct.nombre = params.nombre;
        newProduct.descript = params.descript;
        newProduct.smallDescript = null;
        newProduct.precio = params.precio;
        newProduct.cantidad= params.cantidad;
        newProduct.category = params.category
        category.tipo = newProduct.category;
        newProduct.images = null;
        newProduct.smallImage=null;
        
        CategoryModel.find({tipo:category.tipo}).exec((err,cat)=>{//encontramos las categorias por tipo
            if(err) res.status(500).send({message:'Error en el servidor, no se pueden obtener las categorias'});
            if(cat.length <=0){//puede regresar un array, asi que en caso de que sea 1 significa que existe, sino se agrega
                category.save((error,categorySaved)=>{
                    if(error) res.status(500).send({message:'Error en el servidor, no pudo agregarse la categoria'});
                    if(!categorySaved) res.status(404).send({message:'Error al agregar la categoria'});
                    return res.status(200).send({message:'Categoria agregada'});
                });
            }
        })
        
        newProduct.save((error,productSave)=>{
            if(error) return res.status(500).send({message:'Error al agregar producto'});
            if(!productSave) return res.status(404).send({message:'Error, no se guardo el producto'});
            return res.status(200).send({product:productSave});
        });
    },
    listProduct:(req,res)=>{
        productModel.find({}).exec((err,products)=>{
            if(!products) return res.status(404).send({message:'Error, no se encontraron productos'});
            if (err) return res.status(500).send({message:'Error en el servidor, productos no encontrados'});
            return res.status(200).send({products});
        });
    },
    getProduct:(req,res)=>{
        let productId = req.params.id;
        //en caso de que el productID este vacio, se listaran todos los productos
        if(productId == null) return res.status(200).send({message:'Error, no se encontro el producto'});

        productModel.findById(productId,(err,product)=>{
            //en caso de que no se encuentre un producto con ese id, probablemente no existe o esta malo
            if(!product) return res.status(404).send({message:'Error, no se encontro el producto'});
            if(err) return res.status(500).send({message:'Error en el servidor al encontrar producto'})
            return res.status(200).send({product:product});//regreso el producto
        });
    },
    updateProduct:(req,res)=>{
        let productId = req.params.id;
        let update = req.body; //los datos completos del producto a actualizar
        productModel.findByIdAndUpdate(productId,update,{new:true},(err,producUpdated)=>{
            //pasando la opcion {new:true} actualizara a los datos
            if(productId=='') return res.status(404).send({message:'Error, id vacio'});
            if(err) return res.status(500).send({message:'Error en el servidor, no se pudo actualizar'});
            if(!producUpdated) return res.status(404).send({message:'Error no existe producto a actualizar'});
            return res.status(200).send({producUpdated});
        })
    },
    removeProduct:(req,res)=>{
        let productId = req.params.id;
        productModel.findByIdAndDelete(productId,(err,product)=>{
            if(!productId) return res.status(404).send({message:'Error no se encontro el producto'});
            if(!product) return res.status(404).send({message:'Error, no se encontro producto a eliminar'})
            if(err) return res.status(500).send({message:'Error en el servidor, no se borro el producto'});
            return res.status(200).send({message:product});
        });
    },
    findByCategory:(req,res)=>{
        let category = req.params.category
        CategoryModel.find({tipo:category}).exec((err,cat)=>{
            if(err) return res.status(500).send({message:'Error en el servidor, no se pudo encontrar las categorias'});
            if(cat.length<0){
                return res.status(404).send({message:'Error no se encontro categoria'});
            }else{
                productModel.find({category:category}).exec((err,products)=>{
                    if(products==null) return res.status(404).send({message:'Error, no se encontraron productos'});
                    if(!category) return res.status(404).send({message:'Error, no existe esa categoria'});
                    if(err) return res.status(500).send({message:'Error en el servidor, no se pudieron obtener los datos'});
                    return res.status(200).send({products});
                });
            }
        });
    },

    getCategories:(req,res)=>{
        CategoryModel.find({}).exec((err,products)=>{
            if (err) return res.status(500).send({message:'Error en el servidor, no se encontraron categorias'});
            if(!products) return res.status(404).send({message:'Error, no se encontraron productos'});
            return res.status(200).send({products});
        });
    },

    search:(req,res)=>{
        let product = req.params.product;
        //la forma mas practica fue usar una expresion regular
        productModel.find({nombre:{$regex:product,$options:'i'}}).exec((err,producto)=>{
            if(err) return res.status(500).send({message:'Error en el servidor, No se encontro el producto'});
            if(!producto | producto=='') return res.status(404).send({message:'Error, no se encontro el producto'});
            return res.status(200).send({data:producto});
        })
    },
    uploadImage:(req,res)=>{
        let id = req.params.id
        let fileName = 'Imagen no subida';

        if(req.files){
            //configuracion para la subida de imagenes
            let filePath = req.files.image.path;
            let fileSplit = filePath.split('/');
            let fileName = fileSplit[1];
            let nameSplit = fileName.split('.');
            let ext = nameSplit[1];

            if(ext=='jpeg' | ext=='jpg' | ext=='png'){
                //en caso de que todo este bien que la guarde
                productModel.findByIdAndUpdate(id,{smallImage:fileName},(err,productUploaded)=>{
                    if(err) return res.status(500).send({message:"Error en el servidor,No se pudo subir la imagen"});
                    if(!productUploaded) return res.status(404).send({message:'Error, no se encontro producto a actualizar'});
                    if(!id) return res.status(404).send({message:'Error, id invalido'});
                    return res.status(200).send({productUploaded})
                });
                
            }else{//sino que borre ese archivo
                fs.unlink(filePath,(err)=>{
                    return res.status(403).send({message:'Error, formato incorrecto'});
                })
            }
        }else{
            return res.status(404).send({message:fileName});
        }
    },
    uploadImages:(req,res)=>{
        let id = req.params.id;
        let fileName = 'Imagenes no encontradas';

        if (req.files) {
            let data = req.files.image;
            let filePaths=new Array();
            for (let index = 0; index < data.length; index++) {
                filePaths.push(data[index]);
                
            }
            return  res.status(200).send({data:filePaths.path});
        } else {
            return res.status(404).send({message:'Error no se pudieron subir las imagenes'})
        }
    },
    getImage:(req,res)=>{
        var file = req.params.image;
        var path_file = './uploads/'+file;

        //verficamos si la imagen existe
        fs.exists(path_file,(exists)=>{
            if(exists){//si existe
                return res.sendFile(path.resolve(path_file));// usamos path, para traer esa imagen
                //sendFile, para regresar el archivo
            }else{
                return res.status(200).send({message:'No existe la imagen'});
            }
        })
    }
};

module.exports=productController;