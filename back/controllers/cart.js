'use strict'

let ProductModel = require('../models/product');
let data = {
    item: [],
    total: 0,
    totalProduct: 0
}
function totalPrice(data, cant, producto, producId) {
    if (verifyCart(producId)) {
        data.totalProduct += cant;
        data.total += producto.precio * cant;
    } else {
        data.item.push(producto);
        let precio = producto.precio;
        let total = precio * cant;
        data.total += total;
        data.totalProduct += cant;
    }
}

//todo funcion borrar producto del carro
function deleteProducts(producto, cantidad) {
    if (data.totalProduct > 0) {
        let precio = producto.precio;
        let total = precio * cantidad;
        data.total -= total;
        data.totalProduct -= cantidad;
        if (data.total == 0 & data.totalProduct == 0) {
            data.item.pop(producto._id);
            data.totalProduct = 0;
            data.total = 0;
        }
    }
}

function verifyCart(producId) {
    let isIn = false;
    data.item.forEach(p => {
        if (p._id == producId) {
            isIn = true;
        }
    });
    return isIn;
}
var cartShop = {
    addProduct: (req, res) => {
        let productId = req.params.id;
        let cantidadProduct = parseInt(req.params.cantidad, 10);
        ProductModel.findById(productId, (err, product) => {
            if (err) return res.status(500).send({ message: 'Error en el servidor, no se agrego el producto' });
            if (!product) return res.status(404).send({ message: 'Error, no se encontro el producto' });
            if (!cantidadProduct) return res.status(404).send({ message: 'Error, no se especifico una cantidad' });
            totalPrice(data, cantidadProduct, product, productId);
            return res.status(200).send({ data })
        });
    },
    deleteProduct: (req, res) => {
        let productId = req.params.id;
        let cantidad = req.params.cantidad
        if (!productId | productId == '') return res.status(404).send({ message: 'Error, no se pudo borrar el producto' });
        if (verifyCart(productId)) {
            ProductModel.findById(productId, (err, product) => {
                if (err) return res.status(500).send({ message: 'Error en el servidor, no se encontro producto' });
                if (!product) return res.status(404).send({ message: 'Error, no se encontro el producto' });
                deleteProducts(product, cantidad);
                return res.status(200).send({ data });
            });
        }
    }
}

module.exports = cartShop;