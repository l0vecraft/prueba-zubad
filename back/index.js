'use strict' //habilito modo estricto
/* 
* en caso de que se presente una 'especie de error'
* al no mostrar los datos guardados
* es preferible crear una DB en mongo, con el nombre de la coleccion
* en este caso es Products
*/
let mongoose = require('mongoose');
let app = require('./app');
let port = 3809
mongoose.Promise=global.Promise;// manejaremos todo por medio de promesas
mongoose.connect('mongodb://localhost:27017/backend')// conexion a la db
                .then(()=>{
                    console.log('Conexion Exitosa a la DB');
                    app.listen(port,()=>{
                        console.log('Servidor activo en http://localhost:'+port)
                    })
                }).catch(err => console.log(err));