'use strict'

let express = require('express');
let bodyParser = require('body-parser');

let app = express();

//archivos de rutas
let productRoutes = require('./routes/productRoutes');
//middleware
app.use(bodyParser.urlencoded({extended:false}));//conf necesaria para bodyparser
app.use(bodyParser.json());//convertira todo en json

//cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//rutas
app.use('/api',productRoutes);//http://localhost:3800/api/xxxx
// app.get('/home',(req,res)=>{
//     return res.status(200).send({
//         message:'hola'
//     })
// });
//exports
module.exports = app;